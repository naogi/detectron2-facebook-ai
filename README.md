### detectron2-facebook-ai API

Prediction returns original image with detected objects highlighted

```shell
curl -X POST '<naogi_project_url>/predict' --form 'file=@"/path/to/file"' --output 'output_file'
```
