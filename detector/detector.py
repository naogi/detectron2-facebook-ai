import cv2

from detectron2 import model_zoo
from detectron2.engine import DefaultPredictor
from detectron2.config import get_cfg
from detectron2.structures import Boxes, BoxMode
from detectron2.utils.visualizer import Visualizer
from detectron2.data import MetadataCatalog, DatasetCatalog

class Detector():
  def __init__(self, device='cpu'):
    self.device = device
    threshold = 0.5

    self.cfg = get_cfg()
    self.cfg.merge_from_file(model_zoo.get_config_file("COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x.yaml"))
    self.cfg.MODEL.WEIGHTS = model_zoo.get_checkpoint_url("COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x.yaml")
    self.cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = threshold
    self.cfg.MODEL.DEVICE = self.device

    self.predictor = DefaultPredictor(self.cfg)

  @classmethod
  def prepare_image(cls, image_path):
    return cv2.imread(image_path)

  def predict(self, image):
    return self.predictor(image)

  def visualize(self, outputs, image):
    v = Visualizer(image[:, :, ::-1], MetadataCatalog.get(self.cfg.DATASETS.TRAIN[0]), scale=1.2)
    out = v.draw_instance_predictions(outputs.to(self.device))
    return out.get_image()[:, :, ::-1]
