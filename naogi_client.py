import cv2
import io

from detector.detector import Detector
from naogi import NaogiModel, FileRenderer

class ImageRenderer(FileRenderer):
  @classmethod
  def render(cls, image):
    _, im_buf_arr = cv2.imencode(".jpg", image)
    bytes_io = io.BytesIO(im_buf_arr)
    bytes_io.seek(0)
    return super().render(bytes_io, content_type='image/jpg')

class Model(NaogiModel):
  def load_model(self):
    self.model = Detector('cpu')

  def prepare(self, params_dict):
    self.image = Detector.prepare_image(params_dict['file'])

  def renderer(self):
      return ImageRenderer

  def predict(self):
    outputs = self.model.predict(self.image)
    return self.model.visualize(outputs['instances'], self.image)
